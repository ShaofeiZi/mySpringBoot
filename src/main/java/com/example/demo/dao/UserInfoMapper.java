package com.example.demo.dao;

import com.example.demo.core.universal.Mapper;
import com.example.demo.model.UserInfo;

/**
 * @author 张瑶
 * @Description:
 * @time 2018/4/18 11:54
 */
public interface UserInfoMapper extends Mapper<UserInfo>{
}
